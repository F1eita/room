package com.example.conotes

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.*
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Button
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.conotes.room.Note
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ContactNoteFragment : Fragment() {
    lateinit var etNote: EditText
    lateinit var number: String
    lateinit var noteText: String
    val viewModel: NumberViewModel by activityViewModels()

    private val noteViewModel: NoteViewModel by viewModels {
        ContactViewModelFactory((requireActivity().application as NoteApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_contact_note, container, false)

        val btnSave: Button = fragmentLayout.findViewById(R.id.btnSave)
        val btnDelete: Button = fragmentLayout.findViewById(R.id.btnDelete)

        etNote = fragmentLayout.findViewById(R.id.etNote)

        viewModel.number.observe(requireActivity(), Observer {
            number = it
        })
        noteText = ""
        noteViewModel.allNotes.observe(requireActivity(), Observer {
            for (i in it){
                if (number.equals(i.number)){
                    noteText = i.note
                    break
                }
            }
            etNote.setText(noteText)
        })
        btnSave.setOnClickListener {
            noteText = etNote.getText().toString()
            val note = Note(number, noteText)
            noteViewModel.insert(note)
        }
        btnDelete.setOnClickListener {
            etNote.setText("")
            noteViewModel.deleteNote(number)
        }

        return fragmentLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactNoteFragment()
    }
}