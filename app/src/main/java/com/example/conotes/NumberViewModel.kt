package com.example.conotes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NumberViewModel: ViewModel() {
    private val _number = MutableLiveData<String>("")
    val number: LiveData<String> = _number
    fun setCurrentNumber(number: String){
        _number.value = number
    }

}