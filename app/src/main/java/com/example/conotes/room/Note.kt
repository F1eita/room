package com.example.conotes.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notes_table")
data class Note(@PrimaryKey @ColumnInfo(name = "number") val number: String, @ColumnInfo(name = "note") val note: String) {
}