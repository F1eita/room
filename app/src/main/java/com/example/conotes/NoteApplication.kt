package com.example.conotes

import android.app.Application
import com.example.conotes.room.NoteDB
import com.example.conotes.room.NoteRepository

class NoteApplication : Application() {
    val database by lazy { NoteDB.getDatabase(this) }
    val repository by lazy { NoteRepository(database.noteDao()) }
}