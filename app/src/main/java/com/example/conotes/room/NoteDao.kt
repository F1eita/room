package com.example.conotes.room

import androidx.core.app.NotificationBuilderWithBuilderAccessor
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(note: Note)

    @Query("DELETE FROM notes_table WHERE number = :number")
    suspend fun deleteNote(number: String)


    @Query("SELECT * FROM notes_table")
    fun getAlphabetizedNotes(): Flow<List<Note>>
}