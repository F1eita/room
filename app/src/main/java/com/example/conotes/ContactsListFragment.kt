package com.example.conotes

import android.Manifest.permission.READ_CONTACTS
import android.annotation.SuppressLint
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class ContactsListFragment : Fragment(), ContactsAdapter.Listener  {
    val adapter = ContactsAdapter(this)
    val PERMISSION_REQUEST = 200
    lateinit var navController: NavController

    val viewModel: NumberViewModel by activityViewModels()

    lateinit var rcView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_contacts_list, container, false)
        navController = NavHostFragment.findNavController(this)
        rcView = fragmentLayout.findViewById(R.id.rcView)
        return fragmentLayout
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactsListFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadContacts()
    }

    @SuppressLint("Range")
    fun loadContacts(){
        rcView.layoutManager = LinearLayoutManager(this.context)
        rcView.adapter = adapter
        if (!checkPermission(READ_CONTACTS)){
            Toast.makeText(this.context, "Need contact permission", Toast.LENGTH_SHORT)
        }
        else{
            val cursor = requireActivity().contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null,null)
            cursor?.let{
                while(it.moveToNext()){
                    val name = it.getString(it.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                    val phone = it.getString(it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    val newContact = Contact(name, phone)
                    adapter.addContact(newContact)
                }
            }
            cursor?.close()
        }
    }
    fun checkPermission(permission: String): Boolean{
        return if (Build.VERSION.SDK_INT >= 23 && ContextCompat
                .checkSelfPermission(requireActivity(), permission)
                !=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(permission), PERMISSION_REQUEST)
            false
        }
        else true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ContextCompat.checkSelfPermission(requireActivity(), READ_CONTACTS) == PackageManager.PERMISSION_GRANTED){
            loadContacts()
        }
    }
    override fun onClick(contact: Contact) {
        viewModel.setCurrentNumber(contact.number)
        navController.navigate(R.id.contactNoteFragment)
    }

}