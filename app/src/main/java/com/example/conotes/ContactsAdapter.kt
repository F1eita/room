package com.example.conotes


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ContactsAdapter(val listener: Listener): RecyclerView.Adapter<ContactsAdapter.ContactHolder>() {
    val contactsList = ArrayList<Contact>()
    class ContactHolder(item: View) : RecyclerView.ViewHolder(item){
        var tvName: TextView = item.findViewById(R.id.tvName)
        var tvNumber: TextView = item.findViewById(R.id.tvNumber)
        fun bind(contact: Contact, listener: Listener){
            tvName.setText(contact.name)
            tvNumber.setText(contact.number)
            itemView.setOnClickListener {
                listener.onClick(contact)
            }
        }
    }

    override fun getItemCount(): Int {
        return contactsList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false)
        return ContactHolder(view)
    }

    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        holder.bind(contactsList[position], listener)
    }

    fun addContact(contact: Contact){
        contactsList.add(contact)
        notifyDataSetChanged()
    }

    interface Listener{
        fun onClick(contact: Contact)
    }
}